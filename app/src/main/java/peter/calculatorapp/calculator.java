package peter.calculatorapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class calculator extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);
    }


    ArrayList<String> stringList = new ArrayList<String>();

    String string = "";
    String string1 = "";

    public void getCalculations (View v) {
        TextView calculationView = (TextView) findViewById(R.id.calculationsViews);

        //Get the string from the button value.
        Button button = (Button) v;
        string = (String) button.getText().toString();

        //If the button pressed is not an operator...
        if(!string.contains("+") && !string.contains("-") && !string.contains("x") && !string.contains("u00F7")){

            //If the string from
            string1 = string1+string;

            if(stringList.size()>0){

                //Remove index of last value entered
               stringList.remove((stringList.size()-1));
            }

            stringList.add(string1);
        }else{
            stringList.add(string);
            stringList.add(" ");
            //Clear string after operator is entered to get ready for the next object.
            string1="";
        }

        calculationView.setText(calculationView.getText().toString()+string);
        //Set the calculations screen to the entered values.
        //calculationView.setText(stringList.toString()); <-- for debugging, seeing the [] and commas

    }

    public void calculateResults(View v){
        TextView resultsView = (TextView)findViewById(R.id.resultsView);

        double result = 0;
        int arraySize = stringList.size();

        //Case if only one value is in caculations
        if(arraySize == 1) {
            if(!string.contains("+") && !string.contains("-") && !string.contains("x") && !string.contains("u00F7")){
                result = Double.parseDouble(stringList.get(0));
                resultsView.setText(Double.toString(result));
                return;

            } else { //If the only item in the array is an operator, clear everything. BROKEN?
                clearScreen(v);
                return;
            }

        }else {

            while (arraySize != 1) {
                //If more than 3 items to deal with.
                if (arraySize > 3) {
                    if (stringList.get(3).contains("x") || stringList.get(3).contains("u00F7")) {
                        if (stringList.get(3).contains("x")) {
                            result = Double.parseDouble(stringList.get(2)) * Double.parseDouble(stringList.get(4));
                        }
                        if (stringList.get(3).contains("u00F7")) {
                            result = Double.parseDouble(stringList.get(2)) / Double.parseDouble(stringList.get(4));
                        }

                        stringList.remove(2);
                        stringList.remove(2);
                        stringList.remove(2);
                        stringList.add(2, Double.toString(result));
                        arraySize = stringList.size();
                    } else {
                        if (stringList.get(3).contains("+")) {
                            result = Double.parseDouble(stringList.get(0)) + Double.parseDouble(stringList.get(2));
                        }
                        if (stringList.get(3).contains("-")) {
                            result = Double.parseDouble(stringList.get(0)) - Double.parseDouble(stringList.get(2));
                        }
                        if (stringList.get(3).contains("x")) {
                            result = Double.parseDouble(stringList.get(0)) * Double.parseDouble(stringList.get(2));
                        }
                        if (stringList.get(3).contains("u00F7")) {
                            result = Double.parseDouble(stringList.get(0)) / Double.parseDouble(stringList.get(2));
                        }

                        stringList.remove(0);
                        stringList.remove(0);
                        stringList.remove(0);
                        stringList.add(0, Double.toString(result));
                        arraySize = stringList.size();
                    }
                } else {
                    //If array size is less than 3.

                    if (stringList.get(1).contains("+")) {
                        result = Double.parseDouble(stringList.get(0)) + Double.parseDouble(stringList.get(2));
                    }
                    if (stringList.get(1).contains("-")) {
                        result = Double.parseDouble(stringList.get(0)) - Double.parseDouble(stringList.get(2));
                    }
                    if (stringList.get(1).contains("x")) {
                        result = Double.parseDouble(stringList.get(0)) * Double.parseDouble(stringList.get(2));
                    }
                    if (stringList.get(1).contains("u00F7")) {
                        result = Double.parseDouble(stringList.get(0)) / Double.parseDouble(stringList.get(2));
                    }

                    stringList.remove(0);
                    stringList.remove(0);
                    stringList.remove(0);
                    stringList.add(0, Double.toString(result));
                    arraySize = stringList.size();
                }
            }

            //Set result to the results text view.
            resultsView.setText(Double.toString(result));
        }

    }

    public void clearScreen(View v){
        //Get text views in order to clear them.
        TextView calculationView = (TextView) findViewById(R.id.calculationsViews);
        TextView resultsView = (TextView)findViewById(R.id.resultsView);

        //Clear values for strings
        string = "";
        string1 = "";
        calculationView.setText("");
        resultsView.setText("");
        stringList.clear();
    }

    public void backspace(View v){
        TextView calculationView = (TextView) findViewById(R.id.calculationsViews);
        TextView resultsView = (TextView)findViewById(R.id.resultsView);

        if(stringList.size() > 0) {
            string = "";
            string1 = "";
            stringList.remove((stringList.size() - 1));
            calculationView.setText(stringList.toString()); //When it makes the text field empty is leaves "[]" behind. Fix this?
            resultsView.setText("");
        }
    }

}

